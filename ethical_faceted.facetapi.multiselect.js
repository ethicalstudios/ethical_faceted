//fix for half showing select in facet dropdown Whttp://stackoverflow.com/questions/2638292/after-travelling-back-in-firefox-history-javascript-wont-run
window.onunload = function(){};

(function ($) {

  Drupal.behaviors.EthicalFacetedFacetApiMultiselectWidget = {
    qsVars: [],
    attach: function (context, settings) {

      if($(".facetapi-multiselect").length > 0){
        this.getQueryString();
        $(".facetapi-multiselect").chosen({width: "100%", placeholder_text_multiple: Drupal.t('Select'), placeholder_text_single: Drupal.t('Select')});

        // Go through each facet API multiselect element that is being displayed and add a class to show it's a facet
        jQuery('.facetapi-multiselect', context).each(function () {
          Drupal.behaviors.EthicalFacetedFacetApiMultiselectWidget.addClass(this);
        });

        // Go through each facet API checkbox element that is being displayed and add a class to show it's a facet
        jQuery('.facetapi-facetapi-checkbox-links', context).each(function () {
          Drupal.behaviors.EthicalFacetedFacetApiMultiselectWidget.addClass(this);
        });

        this.wrapForm();
        this.createBreadcrumb();
      }
    },
    wrapForm: function (){
      //add a header before the faceted search dropdowns and checkboxes
      $('.es-facet').first().addClass('first-facet').before('<h2 class="es-facet-header" id="es-filter">' + Drupal.t('Filter') + '</h2>');
      $('.es-facet').last().addClass('last-facet')
    },
    //show the visitor what the page is being filtered by.
    createBreadcrumb: function(){
      var decoded = '';
      var output = '';
      var dURL = window.location.href.split('?')[0];
      var url = '';

      //Look through the querystring for filter values
      jQuery.each(this.qsVars, function (index, value) {

        //build a query string that includes all query string parameters except the current one
        //excluding current one so that the link works to remove the item from the filter
        url = Drupal.behaviors.EthicalFacetedFacetApiMultiselectWidget.buildQueryString(index + '=' + value);
        value = decodeURIComponent(value);

        var match = /f\[[0-9]*\]/.test(index);

        if(match){

          var origValue = value;

          //if there's a filter value then show it to the visitor
          value = value.split(':')[1];
          value = value.replace('[','').replace(']','');

          //http://stackoverflow.com/questions/646628/how-to-check-if-a-string-startswith-another-string
          //if file language facet is in use (used for docs, images, vids when ethical_translation module is enabled)
          if(origValue.lastIndexOf('field_oe_file_language', 0) === 0){
            //get value from faceted select (other wise short version is used, for e.g. 'en' instead of 'English')
            value = $('select.facetapi-multiselect-field-oe-file-language option[value="field_oe_file_language:' + value + '"]').text();
            //http://stackoverflow.com/questions/5631384/remove-everything-after-a-certain-character
            //get rid of number of results at end of string. For e.g. (1)
            value = value.substring(0, value.indexOf(' '));
          }

          output += '<li><a href="' + url + '">' + value + '</a></li>';
        }
        else if(index == 'search_api_views_fulltext' && value != ''){
          output += '<li><a href="' + url + '">' + value + '</a></li>';
        }
      });

      if(output != ''){
        output = '<div class="tags search-tags"><p>' + Drupal.t('Filtered by') + ' </p><ul class="terms">' + output + '</ul></div>';

        if($('.col-md-8 .view-filters').length > 0){
          $('.col-md-8 .view-filters').after(output);
        }
      }

    },
    addClass: function(elm){
      $(elm).closest('div.panel-pane').addClass('es-facet');
    },
    getFacetHeader: function (elm){
      return $(elm).closest('div.panel-pane').children('h2').first().text();
    },
    getQueryString: function(){
      var nvpair = {};
      var qs = window.location.search.replace('?', '');
      var pairs = qs.split('&');
      $.each(pairs, function(i, v){
        var pair = v.split('=');
        nvpair[pair[0]] = pair[1];
      });

      this.qsVars = nvpair;
    },
    //build the query string for each breadcrumb item
    //the item passed in 'exclude' is excluded from the returned query string
    buildQueryString: function(exclude){

      var globalValues = new Array();
      var qs = '';
      var count = 0;
      var dURL = window.location.href.split('?')[0];
      var searchText = jQuery('#edit-search-api-views-fulltext').val();
      var val = '';

      // push on the full search
      if(typeof searchText == 'string' && searchText != ''){
        val = 'search_api_views_fulltext=' + encodeURIComponent(searchText);
        if(val != exclude){
          globalValues.push('search_api_views_fulltext=' + encodeURIComponent(searchText));
        }
      }

      // push on the faceted searches - apart from the passed excluded one
      $("select.facetapi-multiselect[multiple] option:selected").each(function(){
          val = 'f[' + count + ']=' + encodeURIComponent($(this).val());
          if(val != exclude){
            globalValues.push(val);
            count = count + 1;
          }
      });

      jQuery.each(globalValues, function () {
        qs += this + '&';
      });

      //push on any other values that might be on the qs
      for(var propt in this.qsVars){
        var match = /f\[[0-9]*\]/.test(propt);

        if(typeof propt == 'string' && propt != '' && !match && propt != 'search_api_views_fulltext' && propt != 'page'){
          qs += propt + '=' + this.qsVars[propt] + '&';
        }
      }

      if(typeof qs == 'string' && qs != ''){
        qs = '?' + qs.substring(0, qs.length - 1);
      }

      return dURL + qs;
    }
  };

})(jQuery);

